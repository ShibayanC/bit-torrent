# include "master.h"
int nosofSegments = 1;
string segmentsoffile;
string fileMaster;
vector<string> wordVector;
string fileArray[100];


void client_client_Server(int, string, int);
void client_client_ClientMode(string, int, int, int, string);
void client_client_clientserver(int, int, string);
string clientReply_from_server(string);

void dostuff(int, int);
void client(char *, int, int, int, int);

void spawn(int);

void manager(char *name, int mPort, int);
void read_master_conf();

void tracker(char *, int, int, int, int);
string tracker_manager(char *name, int mPort);
void tracker_Server(char *name, int mPort);
string tracker_clientReply(char *buf);

string clientreplyManager();
string client_manager_Mode(char *name, int mPort);
string client_tracker_Mode(char *name, int tPort, string msgS);
void client_client_Mode(string, string, int, int, int, int, string);
//void client_clientserver_req(string);
string readFile(int, string);

void convert()
{
	ifstream is;
	int ctr = 0;
	static const char* const lut = "0123456789ABCDEF";
	is.open(fileMaster.c_str(),ios::binary);
	string lineH, lineHx;
	while (getline(is, lineH))
	{
		size_t len = lineH.length();
		char sep = ' ';
		string output;
		output.reserve(2 * len);
		for (size_t i = 0; i < len; ++i)
		{
			const unsigned char c = lineH[i];
			output.push_back(lut[c >> 4]);
			output.push_back(sep);
			output.push_back(lut[c & 15]);
		}

		fileArray[ctr] = output;
		ctr++;
	}
	is.close();
}

string readFile(int segment, string filename)
{
	int i = 0, count = 0;
	string liner;

	for (i = 0; i < 50; i++)
	{
		if (i == segment)
		{
			liner = fileArray[i];
		}
	}
	string l = "";
	l = liner;
	return l;
}

void calculateSegment(string f)
{
	streampos begin, end;
	ostringstream s;
	string sl, segMreply = "";
	stringstream stringStream(f);
	string line;
	while(getline(stringStream, line))
	{
		size_t prev = 0, pos;
		while ((pos = line.find_first_of("/", prev)) != string::npos)
		{
			if (pos > prev)
				wordVector.push_back(line.substr(prev, pos-prev));
			prev = pos+1;
		}
		if (prev < line.length())
			wordVector.push_back(line.substr(prev, string::npos));
	}
	sl = wordVector[2];
	fileMaster = sl;
	ifstream is;
	is.open(sl.c_str(), ios::binary);
	begin = is.tellg();
	is.seekg (0, ios::end);
	end = is.tellg();
	is.close();
	for (int i = 1; i <= (end - begin)/32; i++)
	{
		s<<i<<" ";
		segmentsoffile = segMreply + s.str();
	}
}

string string_to_hex(const string& input)
{
	static const char* const lut = "0123456789ABCDEF";
	size_t len = input.length();
	char sep = ' ';

	string output;
	output.reserve(2 * len);
	for (size_t i = 0; i < len; ++i)
	{
		const unsigned char c = input[i];
		output.push_back(lut[c >> 4]);
		output.push_back(sep);
		output.push_back(lut[c & 15]);
	}
	return output;
}

void error(const char *msg)
{
	perror(msg);
	exit(0);
}

int main(int argc, char **argv)
{
	system("clear");
	trackerFile.close();
	cout<<"Reading from master.conf\n";
	cout<<"///======================================================///\n";
	read_master_conf();

	int opt;
	while ((opt = getopt (argc, argv, "m:T:")) != -1)  // Manager Port, Tracker Port, Terminal name
	{
		switch (opt)
		{
		case 'm':
			mPort = atoi(optarg);
			break;
		case 'T':
			name = optarg;
			break;
		default:
			cout<<"Parameters can't be read..!!\n";
			abort();
		}
	}
	cout<<"\n-------------------------------------\n";
	cout<<"My Port (MANAGER): "<<mPort<<endl;
	cout<<"-------------------------------------\n";
	for (int k = 0; k < nClient; k++)
	{
		int number = rand() % 9000 + 10000;
		clientPort[k] = number;
	}
	spawn(nClient + 2);
	return 0;
}

void client(char *name, int mPort, int tport, int PID, int PPID)
{
	string msg = "", msgS = "", arr[20] = "\0", filename = "", reply = "";
	int ndID = 0, i=0, cliPort = 0, c, msgType, type, startTime, shareType, dwnstartTime;
	string filetodwnld = "", filetoShare = "";
	msg = client_manager_Mode(name, mPort);
	stringstream ssin(msg);
	while (ssin.good() && i < 20)
	{
		ssin >> arr[i];
		++i;
	}
	ndID = atoi(arr[0].c_str());

	ostringstream f;
	f <<"0"<< arr[0]<<".out";
	filename = f.str();
	ofstream clFile(filename.c_str(), ios::out);
	if (clFile.is_open())
	{
		clFile<<"# 0"<<arr[0]<<".out"<<"\n";
		clFile<<"type"<<" "<<"CLIENT"<<"\n";
		clFile<<"myID: 0"<<arr[0]<<"\n";
		clFile<<"pid:"<<" "<<PID<<" "<<"ppid:"<<" "<<PPID<<"\n";
		clFile<<"tPort:"<<" "<<tPort<<"\n";
		clFile<<"myPort: "<<clientPort[ndID]<<"\n";
		clFile.close();
	}
	else cout<<"Unable to open file\n";
	cliPort = clientPort[ndID];

	filetodwnld = arr[6];
	if ((arr[3] != "-") and (arr[5] == "1")) // Come back here..!!
	{
		c = 1;
		type = 3;
		msgType = 1;
		startTime = atoi(arr[1].c_str());
		dwnstartTime = atoi(arr[4].c_str());
		filetoShare = arr[3];
		calculateSegment(filetoShare);
		convert();
	}
	if ((arr[6] != "-") and (arr[5] == "0"))
	{
		c = 1;
		type = 1;
		msgType = 1;
		startTime = atoi(arr[4].c_str());
		dwnstartTime = atoi(arr[4].c_str());
	}
	if ((arr[6] != "-") and (arr[3] != "-") and (arr[5] == "1"))
	{
		c = 1;
		type = 2;
		msgType = 1;
		startTime = atoi(arr[4].c_str());
		dwnstartTime = atoi(arr[4].c_str());
	}

	filename = f.str();
	clFile.open(filename.c_str(), ios::app);
	if (clFile.is_open())
	{
		clFile<<"\n";
		clFile<<"\n";
		clFile<<"\n";
		clFile<<"###################### \n";
		clFile <<"#Timestamp(sec)	From/To		MessageType		Message_Specific_data\n";
		clFile<<startTime<<" 	"<<"To T"<<" 	"<<"GROUP_SHOW_INTEREST"<<" 	"<<arr[6]<<"\n";
		clFile.close();
	}
	else cout<<"Unable to open file\n";
	ostringstream f1;
	f1 <<msgType<<" "<<arr[0]<<" "<<name<<" "<<cliPort<<" "<<c<<" "
			<<arr[6]<<" "<<type<<" "<<startTime;
	msgS = f1.str();
	shareType = type;
	//cout<<"\nShare Type: "<<type;
	usleep(startTime * 100 * 1000);
	reply = client_tracker_Mode(name, tPort, msgS);
	//cout<<"\nReply: "<<reply;
	client_client_Mode(reply, filename, dwnstartTime, cliPort, ndID, shareType, filetodwnld);
}

void client_client_Mode( string reply, string filename, int startT, int cliPort, int ndID, int sT, string filetodwnld)
{
	int myPort = cliPort;
	int myID = ndID;
	int myshareType = sT;
	string array[20], neighIP;
	int i = 0, count = 1, neighID, neighP;
	stringstream ssin(reply);
	while (ssin.good() && i < 20)
	{
		ssin >> array[i];
		++i;
		count++;
	}

	int k = 4;
	while(k < count)
	{
		neighID = atoi(array[k].c_str());
		k++;
		neighIP = array[k];
		k++;
		neighP = atoi(array[k].c_str());
		k++;
		int flag = 0;
		if ((neighP != -1) and (neighP != cliPort) and (neighP != 0))
		{
			if (neighobjCount == 0)
			{
				nobj[neighobjCount].id = neighID;
				nobj[neighobjCount].ipAddress = neighIP;
				nobj[neighobjCount].port = neighP;
				nobj[neighobjCount].myID = ndID;
				neighobjCount ++;
			}
			else
			{
				for (int i = 0; i < neighobjCount; i++)
				{
					flag = 1;
				}
				if (flag == 0)
				{
					nobj[neighobjCount].id = neighID;
					nobj[neighobjCount].ipAddress = neighIP;
					nobj[neighobjCount].port = neighP;
					nobj[neighobjCount].myID = ndID;
					neighobjCount ++;
				}
			}
		}
	}

	ostringstream idS;
	string id = " ", Line = "";
	for (int i = 0; i < neighobjCount; i++)
	{
		idS << nobj[i].id<<" ";
		id = id + idS.str();
	}
	ofstream clFile(filename.c_str(), ios::app);
	if (clFile.is_open())
	{
		if (id != " ")
			clFile<<startT + 4<<"	"<<"From T"<<"		"<<"GROUP_ASSIGN"<<"		"<<id<<"\n";
		else
			clFile<<startT + 4<<"	"<<"From T"<<"		"<<"GROUP_ASSIGN"<<"		"<<"NULL"<<"\n";
		clFile<<startT + 8<<"	"<<"To 1"<<"	"<<"CLNT_INFO_REQ"<<"	"<<filetodwnld<<"\n";
		clFile.close();
	}

	if (myshareType == 1)	// Can download the file
	{
		usleep(startT * 100 * 1000);
		//cout<<"\nI am here1";
		client_client_ClientMode(filetodwnld, myPort, myID, startT, filename);
	}
	if (myshareType == 2)	// San download and share as well
	{
		usleep(startT * 100 * 1000);
		//cout<<"\nI am here2";
		client_client_clientserver(myPort, startT, filename);
	}
	if (myshareType == 3)	// Can share only
	{
		usleep(startT * 100 * 1000);
		//cout<<"\nI am here3";
		client_client_Server(myPort, filename, startT);
	}
}

void client_client_ClientMode(string filetodwnld, int myPort, int myID, int startT, string filename)
{
	int sock, n;
	string msg_client_client = "", reply = "", msg_Manager = "";
	unsigned int length;
	struct sockaddr_in server, from;
	struct hostent *hp;
	char buffer[1024] = "\0";
	ostringstream cliR;
	sock= socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
		error("socket");

	server.sin_family = AF_INET;
	hp = gethostbyname(name);
	if (hp == 0)
		error("Unknown host");

	stringstream stS(filetodwnld);
	string liS, fL;
	while(getline(stS, liS))
	{
		size_t prev = 0, pos;
		while ((pos = liS.find_first_of("/", prev)) != string::npos)
		{
			if (pos > prev)
				wordVector.push_back(liS.substr(prev, pos-prev));
			prev = pos+1;
		}
		if (prev < liS.length())
			wordVector.push_back(liS.substr(prev, string::npos));
	}
	fL = wordVector[2];

	ostringstream ctg;
	string ct;
	ctg << "file"<<myID<<"-"<<"_csu_cs557_"<<fL;
	ct = ctg.str();								// After slash filename
	clFile.open(ct.c_str(), ios::app);
	if (clFile.is_open())
	{
		clFile.close();
	}

	for (int i = 0; i < neighobjCount; i++)
	{
		//cout<<"\nPort: "<<nobj[i].port;
		bcopy((char *)hp->h_addr, (char *)&server.sin_addr, hp->h_length);
		server.sin_port = htons(nobj[i].port);
		length = sizeof(struct sockaddr_in);

		cliR << "GET"<<" "<<filetodwnld<<" "<<"0";
		msg_client_client = cliR.str();
		strncpy(buffer, msg_client_client.c_str(), 200);
		usleep(10 * 100 * 1000);
		n = sendto(sock,buffer, strlen(buffer),0,(const struct sockaddr *)&server,length);

		if (n < 0)
			error("Sendto");

		n = recvfrom(sock,buffer,1024,0,(struct sockaddr *)&from, &length);

		if (n < 0)
			error("recvfrom");

		n = write(sock,buffer,18);
		cout<<"\n--------------Client---------------";
		cout<<"\nClient Server --> Client: "<<buffer;
		cout<<"\n-----------------------------------";
		clFile.open(filename.c_str(), ios::app);
		if (clFile.is_open())
		{
			clFile<<startT + 18<<"	"<<"To 1"<<"	"<<"CLNT_INFO_REP"<<"	"<<buffer<<"\n";
			clFile.close();
		}
		int m = 0, count = 0;
		stringstream s1(buffer);
		string ArrR[10];
		while (s1.good() && m < 10)
		{
			ArrR[m] = "";
			s1 >> ArrR[m];
			++m;
			count++;
		}
		/*for (int k = 0; k < count; k++)
			cout<<ArrR[k]<<" ";*/
		for (int k = 2; k < count; k++)
		{
			ostringstream c1;
			c1<<"GET"<<" "<<filetodwnld<<" "<<atoi(ArrR[k].c_str());
			//cout<<"\nHii";
			clFile.open(filename.c_str(), ios::app);
			if (clFile.is_open())
			{
				clFile<<startT + 30 + k<<"	"<<"To 1"<<"	"<<"CLNT_SEG_REQ"<<"	"<<c1.str()<<"\n";
				clFile.close();
			}
			msg_client_client = c1.str();
			strncpy(buffer, msg_client_client.c_str(), 200);
			usleep(10 * 100 * 1000);
			n = sendto(sock,buffer, strlen(buffer),0,(const struct sockaddr *)&server,length);
			if (n < 0)
				error("Sendto");

			n = recvfrom(sock,buffer,1024,0,(struct sockaddr *)&from, &length);
			if (n < 0)
				error("recvfrom");

			n = write(sock,buffer,18);
			cout<<"\n--------------Client---------------";
			cout<<"\nClient Server --> Client: "<<buffer;
			cout<<"\n-----------------------------------";
			clFile.open(filename.c_str(), ios::app);
			if (clFile.is_open())
			{
				clFile<<startT + 35 + k<<"	"<<"From 1"<<"	"<<"CLNT_SEG_REP"<<"	"<<buffer<<"\n";
				clFile.close();
			}
			clFile.open(ct.c_str(), ios::app);
			if (clFile.is_open())
			{
				clFile << buffer;
				clFile.close();
			}
		}
		close(sock);
	}
}

void client_client_Server(int myPort, string filename, int startT)
{
	int sock, length, n, k = 2;
	socklen_t fromlen;
	struct sockaddr_in server;
	struct sockaddr_in from;
	string msg = "";
	string Buffer = "";
	string line = "";
	char buf[1024] = "\0", reply[1024] = "\0";

	sock=socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) error("Opening socket");
	length = sizeof(server);
	bzero(&server,length);
	server.sin_family=AF_INET;
	server.sin_addr.s_addr=INADDR_ANY;
	server.sin_port=htons(myPort);

	if (bind(sock,(struct sockaddr *)&server,length)<0)
		error("binding");
	fromlen = sizeof(struct sockaddr_in);

	while (1)
	{
		n = recvfrom(sock, buf, 1024, 0, (struct sockaddr *)&from, &fromlen);
		if (n < 0)
			error("recvfrom");

		cout<<"\n-----------Client Server----------";
		cout<<"\nClient --> Client Server Message: "<<buf;
		cout<<"\n----------------------------------";
		Buffer = buf;
		clFile.open(filename.c_str(), ios::app);
		if (clFile.is_open())
		{
			clFile<<startT + 30 + k<<"	"<<"To 1"<<"	"<<"CLNT_SEG_REQ"<<"	"<<Buffer<<"\n";
			clFile.close();
		}
		k ++;
		msg = clientReply_from_server(Buffer);

		strcpy(reply, msg.c_str());
		clFile.open(filename.c_str(), ios::app);
		if (clFile.is_open())
		{
			clFile<<startT + 30 + k<<"	"<<"From 1"<<"	"<<"CLNT_SEG_REP"<<"	"<<msg<<"\n";
			clFile.close();
		}
		n = sendto(sock, reply, 1024, 0, (struct sockaddr *)&from, fromlen);
		k++;
		if (n  < 0)
			error("sendto");
	}
}

string clientReply_from_server(string Buf)
{
	int i = 0, segMnt;
	string array1[30], fileN, msg = "", replyline = "";
	stringstream ssin(Buf);
	ostringstream s2;
	while (ssin.good() && i < 50)
	{
		ssin >> array1[i];
		++i;
		count++;
	}

	fileN = array1[1];
	segMnt = atoi(array1[2].c_str());
	if (segMnt == 0)
	{
		usleep(10 * 100 * 1000);
		//cout<<"\nFilename: "<<fileN<<" "<<segMnt;
		s2 <<"GETOK"<<" "<<fileN<<" "<<segmentsoffile;
		msg = s2.str();
	}
	else
	{
		replyline = readFile(segMnt, fileN);
		s2 <<replyline;  /*  Start coding from here */
		msg = s2.str();
	}
	return msg;
}

void client_client_clientserver(int myPort, int startT, string filename)  // Here is the method
{
	int sock, length, n, k = 2;
	socklen_t fromlen;
	struct sockaddr_in server;
	struct sockaddr_in from;
	string msg = "";
	string Buffer = "";
	char buf[1024] = "\0", reply[1024] = "\0";

	sock=socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) error("Opening socket");
	length = sizeof(server);
	bzero(&server,length);
	server.sin_family=AF_INET;
	server.sin_addr.s_addr=INADDR_ANY;
	server.sin_port=htons(myPort);

	if (bind(sock,(struct sockaddr *)&server,length)<0)
		error("binding");
	fromlen = sizeof(struct sockaddr_in);
	while (1)
	{
		n = recvfrom(sock, buf, 1024, 0, (struct sockaddr *)&from, &fromlen);
		if (n < 0)
			error("recvfrom");
		cout<<"\n-----------Client Server----------";
		cout<<"\nClient --> Client Server Message: "<<buf;
		cout<<"\n----------------------------------";
		Buffer = buf;
		clFile.open(filename.c_str(), ios::app);
		if (clFile.is_open())
		{
			clFile<<startT + 30 + k<<"	"<<"To 1"<<"	"<<"CLNT_SEG_REQ"<<"	"<<Buffer<<"\n";
			clFile.close();
		}
		k ++;
		msg = clientReply_from_server(Buffer);

		strcpy(reply, msg.c_str());
		clFile.open(filename.c_str(), ios::app);
		if (clFile.is_open())
		{
			clFile<<startT + 30 + k<<"	"<<"From 1"<<"	"<<"CLNT_SEG_REP"<<"	"<<msg<<"\n";
			clFile.close();
		}
		n = sendto(sock, reply, 1024, 0, (struct sockaddr *)&from, fromlen);
		k++;
		if (n  < 0)
			error("sendto");
	}
}

string client_tracker_Mode(char *name, int tPort, string msgS)
{
	/*-- Client UDP client starts here --*/
	int sock, n;
	unsigned int length;
	struct sockaddr_in server, from;
	struct hostent *hp;
	char buffer[1024] = "\0";

	sock= socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
		error("socket");
	//cout<<"\nI am here for you..!!";
	server.sin_family = AF_INET;
	hp = gethostbyname(name);
	if (hp == 0)
		error("Unknown host");

	bcopy((char *)hp->h_addr, (char *)&server.sin_addr, hp->h_length);
	server.sin_port = htons(tPort);
	length = sizeof(struct sockaddr_in);

	strncpy(buffer, msgS.c_str(), 200);
	n = sendto(sock,buffer, strlen(buffer),0,(const struct sockaddr *)&server,length);

	if (n < 0)
		error("Sendto");

	n = recvfrom(sock,buffer,1024,0,(struct sockaddr *)&from, &length);

	if (n < 0)
		error("recvfrom");

	n = write(sock,buffer,18);
	cout<<"\n-----------Client-----------";
	cout<<"\nTracker --> Client: "<<buffer;
	cout<<"\n----------------------------";
	close(sock);
	string reply;
	reply = buffer;
	return reply;
	/*-- Client UDP client ends here --*/
}

void tracker(char *name, int mPort, int tPort, int PID, int PPID)
{
	string line;
	trackerFile.open ("tracker.out", ios::app);
	line = tracker_manager(name, mPort);
	if (trackerFile.is_open())
	{
		trackerFile<<"# tracker.out"<<"\n";
		trackerFile<<"type"<<" "<<"TRACKER"<<"\n";
		trackerFile<<"pid:"<<" "<<PID<<" "<<"ppid:"<<" "<<PPID<<"\n";
		trackerFile<<"tPort:"<<" "<<tPort<<"\n";
		trackerFile.close();
	}
	else cout<<"Unable to open file\n";
	//cout<<line;
	tracker_Server(name, tPort);
}

void tracker_Server(char *name, int tPort)
{
	int sock, length, n;
	socklen_t fromlen;
	struct sockaddr_in server;
	struct sockaddr_in from;
	string msg = "";
	char buf[1024] = "\0", reply[1024] = "\0";

	sock=socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) error("Opening socket");
	length = sizeof(server);
	bzero(&server,length);
	server.sin_family=AF_INET;
	server.sin_addr.s_addr=INADDR_ANY;
	server.sin_port=htons(tPort);

	if (bind(sock,(struct sockaddr *)&server,length)<0)
		error("binding");
	fromlen = sizeof(struct sockaddr_in);
	while (1)
	{
		n = recvfrom(sock, buf, 1024, 0, (struct sockaddr *)&from, &fromlen);
		if (n < 0)
			error("recvfrom");
		cout<<"\n------------Tracker-----------------";
		cout<<"\nClient --> Tracker: "<<buf;
		cout<<"\n------------------------------------";

		msg = tracker_clientReply(buf);
		strcpy(reply, msg.c_str());
		n = sendto(sock, reply, 1024, 0, (struct sockaddr *)&from, fromlen);
		if (n  < 0)
			error("sendto");
	}
}

string tracker_clientReply(char *buf)
{
	//cout<<"\nI am here "<<buf;
	string reply = "", Arr[20], rep = "", hx = "", idipPort = "", r = "";
	int i = 0, neighCount = 0, grp = 1, mgT = 2, fC = 1, nC = 1, g;
	rep = buf;
	hx = string_to_hex(rep);
	stringstream sin(rep);
	while (sin.good() && i < 20)
	{
		sin >> Arr[i];
		i++;
	}
	//cout<<"\nI am here 1"<<Arr[6];
	ofstream trackerFile("tracker.out", ios::app);
	if (trackerFile.is_open())
	{
		trackerFile<<"\n";
		trackerFile<<"\n# ClientID"<<" -> "<<"TimeReceived"<<" -> "<<"Filename";
		trackerFile<<"\n      "<<Arr[1]<<"       "<<Arr[7]<<"        "<<Arr[5];
		trackerFile<<"\n---"<<hx<<"---\n";
		trackerFile.close();
	}
	ostringstream f2;
	stringstream P;
	int T = atoi(Arr[6].c_str());

	r = "";
	int fl = 0, index = 0;
	if (Arr[5] != "-")
	{
		if (fileobjCount == 0)
		{
			//cout<<"\nT: "<<T;
			if (T == 1)
			{

				fobj[fileobjCount].name = Arr[5];
				fobj[fileobjCount].group = grp;
				fobj[fileobjCount].fileCount = fC;
				fobj[fileobjCount].nCount = nC;

				obj[objCount].filename = Arr[5];
				obj[objCount].ipAddress = Arr[2];
				obj[objCount].port = -1;
				obj[objCount].grp = grp;
				obj[objCount].id = atoi(Arr[1].c_str());
				f2 << mgT <<" "<<fobj[fileobjCount].fileCount<<" "<<fobj[fileobjCount].name<<" "
						<<fobj[fileobjCount].nCount<<" "<<obj[objCount].id<<" "<<obj[objCount].ipAddress<<" "<<obj[objCount].port;
				reply = f2.str();
			}
			if (T == 2)
			{
				//cout<<"I am here 2"<<endl;
				fobj[fileobjCount].name = Arr[5];
				fobj[fileobjCount].group = grp;
				fobj[fileobjCount].fileCount = fC;
				fobj[fileobjCount].nCount = nC;

				obj[objCount].filename = Arr[5];
				obj[objCount].grp = grp;
				obj[objCount].id = atoi(Arr[1].c_str());
				obj[objCount].ipAddress = Arr[2];
				obj[objCount].port = atoi(Arr[3].c_str());

				f2 << mgT <<" "<<fobj[fileobjCount].fileCount<<" "<<fobj[fileobjCount].name<<" "
						<<fobj[fileobjCount].nCount<<" "<<obj[objCount].id<<" "<<obj[objCount].ipAddress<<" "<<obj[objCount].port;
				reply = f2.str();
			}
			if (T == 3)
			{
				fobj[fileobjCount].name = Arr[5];
				fobj[fileobjCount].group = -1;
				fobj[fileobjCount].fileCount = fC;
				fobj[fileobjCount].nCount = nC;

				obj[objCount].filename = Arr[5];
				obj[objCount].grp = -1;
				obj[objCount].id = atoi(Arr[1].c_str());
				obj[objCount].ipAddress = Arr[2];
				obj[objCount].port = atoi(Arr[3].c_str());

				f2 << mgT <<" "<<fobj[fileobjCount].fileCount<<" "<<fobj[fileobjCount].name<<" "
						<<fobj[fileobjCount].nCount<<" "<<obj[objCount].id<<" "<<obj[objCount].ipAddress<<" "<<obj[objCount].port;
				//cout<<"I am here 3"<<endl;
				reply = f2.str();
			}
			fileobjCount ++;
			grp ++;
		}
		else
		{

			for (int i = 0; i < fileobjCount; i ++)
			{
				if (fobj[i].name == Arr[5])
				{
					index = i;
					fl = 1;
				}
			}
			if (fl == 1)
			{
				if (T == 1)
				{
					r = "";
					g = fobj[index].group;
					for (int i = 0; i < objCount; i++)
					{
						if (obj[i].grp == g)
						{
							P << obj[i].id<<" "<<obj[i].ipAddress<<" "<<obj[i].port<<" ";
							//cout<<"I am here 4"<<endl;
							r = r + P.str();
						}
					}
					obj[objCount].ipAddress = Arr[2];
					obj[objCount].port = -1;
					obj[objCount].id = -1;
					obj[objCount].grp = g;
					obj[objCount].filename = Arr[5];

					f2 <<mgT<<" "<<fobj[index].fileCount<<" "<<fobj[index].name<<" "
							<<fobj[index].nCount<<" "<<r;
					reply = f2.str();
					//cout<<"I am here 4"<<endl;
				}

				if (T == 2)
				{
					r = "";
					g = fobj[index].group;
					fobj[index].nCount = fobj[index].nCount + 1;
					fobj[index].fileCount = fobj[index].fileCount + 1;
					for (int i = 0; i < objCount; i++)
					{
						if (obj[i].grp == g)
						{
							P << obj[i].id<<" "<<obj[i].ipAddress<<" "<<obj[i].port;
							r = r + P.str();
						}
					}
					obj[objCount].ipAddress = Arr[2];
					obj[objCount].id = atoi(Arr[1].c_str());
					obj[objCount].port = atoi(Arr[3].c_str());
					obj[objCount].filename = Arr[5];
					obj[objCount].grp = g;

					f2 <<mgT<<" "<<fobj[index].fileCount<<" "<<fobj[index].name<<" "
							<<fobj[index].nCount<<" "<<r;
					//cout<<"I am here 5"<<endl;
					reply = f2.str();
				}

				if (T == 3)
				{
					g = fobj[index].group;
					fobj[index].nCount = fobj[index].nCount + 1;
					fobj[index].fileCount = fobj[index].fileCount + 1;

					obj[objCount].ipAddress = Arr[2];
					obj[objCount].id = atoi(Arr[1].c_str());
					obj[objCount].port = atoi(Arr[3].c_str());
					obj[objCount].grp = g;
					for (int i = 0; i < objCount; i++)
					{
						if (obj[i].grp == g)
						{
							P << obj[i].id<<" "<<obj[i].ipAddress<<" "<<obj[i].port<<" ";
							r = r + P.str();
						}
					}
					f2 <<mgT<<" "<<fobj[index].fileCount<<" "<<fobj[index].name<<" "
							<<fobj[index].nCount<<" "<<r;
					//cout<<"\nI am here 6"<<endl;
					reply = f2.str();
				}
			}
			else
			{
				if (T == 1)
				{
					fobj[fileobjCount].name = Arr[5];
					fobj[fileobjCount].group = grp;
					fobj[fileobjCount].fileCount = fC;
					fobj[fileobjCount].nCount = nC;

					obj[objCount].filename = Arr[5];
					obj[objCount].ipAddress = Arr[2];
					obj[objCount].port = -1;
					obj[objCount].grp = grp;
					obj[objCount].id = atoi(Arr[1].c_str());
					f2 << mgT <<" "<<fobj[fileobjCount].fileCount<<" "<<fobj[fileobjCount].name<<" "
							<<fobj[fileobjCount].nCount<<" "<<obj[objCount].id<<" "<<obj[objCount].ipAddress<<" "<<obj[objCount].port;
					reply = f2.str();
					//cout<<"I am here 7"<<f2.str()<<endl;
				}
				if (T == 2)
				{
					fobj[fileobjCount].name = Arr[5];
					fobj[fileobjCount].group = grp;
					fobj[fileobjCount].fileCount = fC;
					fobj[fileobjCount].nCount = nC;

					obj[objCount].filename = Arr[5];
					obj[objCount].grp = grp;
					obj[objCount].id = atoi(Arr[1].c_str());
					obj[objCount].ipAddress = Arr[2];
					obj[objCount].port = atoi(Arr[3].c_str());

					f2 << mgT <<" "<<fobj[fileobjCount].fileCount<<" "<<fobj[fileobjCount].name<<" "
							<<fobj[fileobjCount].nCount<<" "<<obj[objCount].id<<" "<<obj[objCount].ipAddress<<" "<<obj[objCount].port;
					reply = f2.str();
					//cout<<"I am here 8"<<endl;
				}
				if (T == 3)
				{
					fobj[fileobjCount].name = Arr[5];
					fobj[fileobjCount].group = -1;
					fobj[fileobjCount].fileCount = fC;
					fobj[fileobjCount].nCount = nC;

					obj[objCount].filename = Arr[5];
					obj[objCount].grp = grp;
					obj[objCount].id = atoi(Arr[1].c_str());
					obj[objCount].ipAddress = Arr[2];
					obj[objCount].port = atoi(Arr[3].c_str());

					f2 << mgT <<" "<<fobj[fileobjCount].fileCount<<" "<<fobj[fileobjCount].name<<" "
							<<fobj[fileobjCount].nCount<<" "<<obj[objCount].id<<" "<<obj[objCount].ipAddress<<" "<<obj[objCount].port;
					reply = f2.str();
					//cout<<"I am here 9"<<endl;
				}
				fileobjCount ++;
				grp ++;
			}
		}
	}
	objCount ++;
	return reply;
}

void read_master_conf()
{
	char buf[MAX_CHARS_PER_LINE];
	const char* token[MAX_TOKENS_PER_LINE] = {};
	int flag = 0;
	ifstream myFile;
	int tokenC = 0;
	myFile.open("manager.conf");
	if (!myFile.good())
	{
		cout<<"\nCannot read file..!!";
		exit(0);
	}
	while (!myFile.eof())
	{
		char buf[MAX_CHARS_PER_LINE];
		myFile.getline(buf, MAX_CHARS_PER_LINE);

		int n = 0;

		const char* token[MAX_TOKENS_PER_LINE] = {};
		const char* token1[MAX_TOKENS_PER_LINE] = {};
		token[0] = strtok(buf, delimSp);
		if (token[0])
		{
			if ((strcmp(token[0],"#") != 0) and (token[0] != '\0'))
			{
				tokenC ++;
				for (n = 1; n < MAX_TOKENS_PER_LINE; n++)
				{
					token[n] = strtok(0, delimSp);
					if (!token[n]) break;
				}
			}
		}

		if (tokenC == 1)
		{
			for (int i = 0; i < n; i++)
				nClient = atoi(token[i]);
		}
		if (tokenC == 2)
		{
			for (int i = 0; i < n; i++)
				timeOut = atoi(token[i]);
		}
		if (tokenC >= 3)
		{
			if (tokenC <= (2+nClient))
			{
				for (int i = 0; i < n; i++)
				{
					nodeID[counter] = atoi(token[i]);
					line[counter] = token[i];
				}
				counter++;
			}
		}
		if (tokenC > (3 + nClient))
		{
			for (int i = 0; i < n; i++)
			{
				if (flag == 0)
				{
					if (atoi(token[i]) != -1)
					{
						nodeidInit[counter1] = atoi(token[i]);
						line1[counter1] = token[i];
						counter1 ++;
					}
					else
						flag = 1;

				}
				else
				{
					if ((atoi(token[i]) != -1) and (token[i] != '\0'))
					{
						nodeidDwnld[counter2] = atoi(token[i]);
						line2[counter2] = token[i];
						counter2 ++;
					}
				}
			}
		}
	}

	cout<<"\n# of Clients: "<<nClient;
	cout<<"\nTime-Out: "<<timeOut;

	for (int k = 0; k < counter; k++)
	{
		string data;
		int val1 = 0, val2 = 0;

		istringstream iss(line[k]);
		getline(iss, data, '\t');
		iss >> val1 >> val2;
		pktDelay[k] = val1;
		pktdrpProb[k] = val2;
	}
	cout<<"\nNode ID:Pkt delay:Pkt Drop Prob"<<endl;
	for (int k = 0; k < counter; k++)
		cout<<nodeID[k]<<"	"<<pktDelay[k]<<"	"<<pktdrpProb[k]<<endl;

	for (int k = 0; k < counter1; k++)
	{
		string data;
		char l[50];

		istringstream iss(line1[k]);
		getline(iss, data, '\t');
		iss >> l;
		strcpy(filesInit[k], l);
	}

	cout<<"\nNodes having files initially";
	cout<<"\n------------------------------------";
	cout<<"\nNodeID  :  File Location \n";
	for (int k = 0; k < counter1; k++)
		cout<<nodeidInit[k]<<"   "<<filesInit[k]<<endl;

	for (int k = 0; k < counter2; k++)
	{
		string data;
		char l[50];
		int val3, val4;

		istringstream iss(line2[k]);
		getline(iss, data, '\t');
		iss >> l >> val3 >> val4;
		strcpy(filesDwnld[k], l);
		starttimeDwnld[k] = val3;
		shareDwnld[k] = val4;
	}

	cout<<"\nNodes downloading files";
	cout<<"\n------------------------------------";
	cout<<"\nNodeID  :  File Name  :  Start-Time  :  Condition\n";
	for (int k = 0; k < counter2; k++)
		cout<<nodeidDwnld[k]<<"   "<<filesDwnld[k]<<"   "<<starttimeDwnld[k]<<"   "<<shareDwnld[k]<<endl;
	cout<<"\n///=========================================================///";
}

void spawn(int n)
{
	int i, PID, PPID, trPID;
	i = pfork(n);
	if (i == 0)
	{
		cout<<"\nMANAGER : pid -> "<<getpid()<<" ppid -> "<<getppid()<<endl;
		PID = getpid();
		manager(name, mPort, PID);
	}
	if (i == 1)
	{
		cout<<"\nTRACKER : pid -> "<<getpid()<<" ppid -> "<<getppid()<<endl;
		PID = getpid();
		PPID = getppid();
		trPID = PPID;
		tracker(name, mPort, tPort, PID, PPID);
	}
	if (i == 2)
	{
		cout<<"\nCLIENT : pid -> "<<getpid()<<" ppid -> "<<getppid()<<endl;
		PID = getpid();
		PPID = getppid();
		client(name, mPort, tPort, PID, PPID);
		usleep(10 * 100 * 1000);

	}
	if (i == 3)
	{
		cout<<"\nCLIENT : pid -> "<<getpid()<<" ppid -> "<<getppid()<<endl;
		PID = getpid();
		PPID = getppid();
		client(name, mPort, tPort, PID, PPID);
		usleep(10 * 100 * 1000);
	}
	if (i == 4)
	{
		cout<<"\nCLIENT : pid -> "<<getpid()<<" ppid -> "<<getppid()<<endl;
		PID = getpid();
		PPID = getppid();
		client(name, mPort, tPort, PID, PPID);
		usleep(10 * 100 * 1000);
	}/*
	if (i == 5)
	{
		cout<<"\nCLIENT : pid -> "<<getpid()<<" ppid -> "<<getppid()<<endl;
		usleep(10 * 100 * 1000);
		PID = getpid();
		PPID = getppid();
		client(name, mPort, tPort, PID, PPID);
	}
	if (i == 6)
	{
		cout<<"\nCLIENT : pid -> "<<getpid()<<" ppid -> "<<getppid()<<endl;
		usleep(10 * 100 * 1000);
		PID = getpid();
		PPID = getppid();
		client(name, mPort, tPort, PID, PPID);
	}*/
}

int pfork(int x)
{
	int j;
	for (j = 1; j < x; j++)
	{
		if (fork() == 0)
		{
			return j;
		}
	}
	return 0;
}

void manager(char *name, int portno, int PID)
{
	int sockfd, newsockfd, pid;
	socklen_t clilen;
	struct sockaddr_in serv_addr, cli_addr;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
		error("ERROR opening socket");
	bzero((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		error("ERROR on binding");
	listen(sockfd, 10);

	clilen = sizeof(cli_addr);
	while(1)
	{
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		dostuff(newsockfd, PID);
	}
}

void dostuff (int sock, int PID)
{
	int n;
	char buffer[256] = "\0";
	char msg[256] = "\0";
	const char *l;
	string reply = "";
	string line = "";

	bzero(buffer,256);
	n = read(sock,buffer,255);
	if (n < 0) error("ERROR reading from socket");

	line = buffer;
	string data;
	int val1;

	istringstream iss(line);
	getline(iss, data, ' ');
	if (data == "TRACKER")
	{
		iss >> val1;
		cout<<"\n-------------Manager-----------";
		cout<<"\nTracker --> Manager: "<<buffer;
		cout<<"\n-------------------------------";
		tPort = val1;
		strcpy(msg, "TRACKER OK");

		n = write(sock,msg,18);
		if (n < 0) error("ERROR writing to socket");
	}

	if (data == "CLIENT")
	{
		cout<<"\n------------Manager--------------";
		cout<<"\nClient --> Manager: "<<buffer;
		cout<<"\n---------------------------------";
		reply = clientreplyManager();
		strncpy(msg, reply.c_str(), sizeof(msg));
		n = write(sock,msg,50);
		if (n < 0) error("ERROR writing to socket");
	}
}

string tracker_manager(char *name, int mPort)
{
	/*--TCP Client Mode for contacting manager--*/
	int sockfd, portno, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;

	char buffer[1024] = "\0";
	portno = htons(mPort);

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		perror("ERROR opening socket");
		exit(1);
	}
	server = gethostbyname(name);

	if (server == '\0')
	{
		fprintf(stderr,"ERROR, no such host\n");
		exit(0);
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(mPort);

	if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
	{
		perror("ERROR connecting");
		exit(1);
	}

	char msg[256] = "TRACKER 11000";
	strncpy(buffer, msg, 20);

	//cout<<"\nTRACKER -> MANAGER: "<<buffer<<endl;
	n = write(sockfd,buffer,strlen(buffer));

	if (n < 0)
	{
		perror("ERROR writing to socket");
		exit(1);
	}

	bzero(buffer,256);
	n = read(sockfd,buffer,255);

	if (n < 0)
	{
		perror("ERROR reading from socket");
		exit(1);
	}
	cout<<"\n------------Tracker----------------";
	cout<<"\nManager-->Tracker: "<<buffer;
	cout<<"\n-----------------------------------";
	close(sockfd);
	return buffer;
	/*--TCP Client Mode switched off--*/
}

string clientreplyManager()   // Come back in this loop
{
	int clientID, pktD, pktdrpP, strtT, share;
	char msg[256], initF[30], dwnldF[30];
	string reply = "";
	ostringstream line;

	if (clientCount <= nClient)
	{
		for (int k = 0; k < counter; k++)
		{
			if (clientCount == nodeID[k])
			{
				clientID = nodeID[k];
				pktD = pktDelay[k];
				pktdrpP = pktdrpProb[k];
			}
		}
		int f1 = 0;
		for (int k = 0; k < counter1; k++)
		{
			if (clientID == nodeidInit[k])
			{
				strcpy(initF, filesInit[k]);
				f1 = 1;
			}
		}
		if (f1 == 0)
			strcpy(initF, "-");

		int f2 = 0;
		for (int k = 0; k < counter2; k++)
		{
			if (clientID == nodeidDwnld[k])
			{
				strtT = starttimeDwnld[k];
				share = shareDwnld[k];
				strcpy(dwnldF, filesDwnld[k]);
				f2 = 1;
			}
		}
		if (f2 == 0)
		{
			strtT = 0;
			share = 0;
			strcpy(dwnldF, "-");
		}
		line <<clientID<<" "<<pktD<<" "<<pktdrpP<<" "
				<<initF<<" "<<strtT<<" "<<share<<" "<<dwnldF;
		reply = line.str();
	}
	clientCount++;
	return reply;
}

string client_manager_Mode(char *name, int mPort)
{
	/*-- Client TCP client mode starts--*/
	int sockfd, portno, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;

	char buffer[1024] = "\0";
	portno = htons(mPort);

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		perror("ERROR opening socket");
		exit(1);
	}
	server = gethostbyname(name);

	if (server == '\0')
	{
		fprintf(stderr,"ERROR, no such host\n");
		exit(0);
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(mPort);

	if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
	{
		perror("ERROR connecting");
		exit(1);
	}

	char msg[256] = "CLIENT REG";
	strcpy(buffer, msg);

	n = write(sockfd,buffer,strlen(buffer));

	if (n < 0)
	{
		perror("ERROR writing to socket");
		exit(1);
	}

	bzero(buffer,256);
	n = read(sockfd,buffer,255);

	if (n < 0)
	{
		perror("ERROR reading from socket");
		exit(1);
	}
	cout<<"\n----------CLIENT----------";
	cout<<"\nManager --> Client: "<<buffer;
	cout<<"\n--------------------------";
	clientReply[clientR] = buffer;
	clientR ++;
	close(sockfd);
	return buffer;
	/*-- Client TCP client mode ends here--*/
}
