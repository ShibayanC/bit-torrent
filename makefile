OBJS = master.o
CC = g++
DEBUG = -g
CFLAGS = -Wall -c $(DEBUG)
LFLAG = -Wall $(DEBUG)

h2 : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o master

master.o: master.h master.cpp
	$(CC) $(CFLAGS) master.cpp

clean:
	-rm -f p1 *.o 

tar:
	tar cfv p1.shibayan.tar master.h master.cpp makefile README manager.conf foo
